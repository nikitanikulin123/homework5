//Задание №1
var students = [
'Алексей Петров', 0,
'Ирина Овчинникова', 60,
'Глеб Стукалов', 30,
'Антон Павлович', 30,
'Виктория Заровская', 30,
'Алексей Левенец', 70,
'Тимур Вамуш', 30,
'Евгений Прочан', 60,
'Александр Малов', 0
];
var studentsList = [];
for(var i = 1; i < students.length; i += 2) {
    studentsList.push('Студент ' + students[i - 1] + ' набрал ' + students[i] + ' баллов');
}
console.log('Список студентов: ' + studentsList);

//Задание №2
var maxName = '';
var maxNumber = 0;
for(var i = 1; i < students.length; i += 2) {
    if (maxNumber < students[i]) {
        maxNumber = students[i];
        maxName = students[i - 1];
    }
}
console.log('Студент набравший максимальный балл:');
console.log('Студент ' + maxName + ' имеет максимальный балл ' + maxNumber);

//Задание №3
students.push('Николай Фролов', 0, 'Олег Боровой', 0);

//Задание №4
//for(var i = 0; i < students.length; i++) {
//    if(students[i] == 'Антон Павлович' || students[i] == 'Николай Фролов')
//        students[i + 1] += 10;
//}
//console.log(students);

//var student1 = students.findIndex(function (x) {
//   return x === 'Антон Павлович';
//});
//var student2 = students.findIndex(function (x) {
//    return x === 'Николай Фролов';
//});
//students[student1 + 1] += 10;
//students[student2 + 1] += 10;

//students[7] += 10;
//students[19] += 10;

var student1 = students.indexOf('Антон Павлович');
var student2 = students.indexOf('Олег Боровой');

students[student1 + 1] += 10;
students[student2 + 1] += 10;

//Задание №5
var studentsWithNoPoints = [];
for(var i = 1; i < students.length; i += 2) {
    if(students[i] == 0)
        studentsWithNoPoints.push(students[i - 1]);
}
console.log('Студенты не набравшие баллов: ' + studentsWithNoPoints);

// Дополнительное Задание №6
for(var i = 1; i < students.length; i += 2) {
    if(students[i] === 0) {
        students.splice(i - 1, 2);
        i -= 2;
    }
}
console.log(students);
